## Purple Cow

#### Pre-requities

- [Node.js](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/en/)
#### Installation 

- Run ```yarn install``` to install all dependencies. 
 
### Usage

#### Port
The standard listen port is 3000 when running locally (i.e., http://localhost:3000).  To update the listen port to a different value, update the the value of the ```port:``` parameter found in the webpack.config.js.

#### Development
Run the following command to run the application locally:

```
yarn start
```

#### Production 

```
yarn build
```

#### Docker
Building docker image:

```
docker build -t purple-cow -f Dockerfile .
```

Running container:

```
docker run -it -p 3000:3000 purple-cow
```
