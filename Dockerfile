# pull the base image
FROM node:current-alpine

# set the working direction
WORKDIR /app

# install app dependencies
COPY package.json ./

COPY package-lock.json ./

RUN yarn install

# add app
COPY . ./

EXPOSE 3000

# start app
CMD yarn start

