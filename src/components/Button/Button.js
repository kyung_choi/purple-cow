import React from 'react';
import PropTypes from 'prop-types';

const buttonPropTypes = {
    onClick: PropTypes.func, 
    displayLabel: PropTypes.string
  };
  
const buttonDefaultProps = {
    onClick: null,
    displayLabel: null
};

const Button = props => {
    const { onClick, displayLabel } = props;
    return (
    <button size="default" onClick={onClick}>
        {displayLabel}
    </button>
  );
};

Button.propTypes = buttonPropTypes;
Button.defaultProps = buttonDefaultProps;

export default Button;