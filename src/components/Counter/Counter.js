import React, { useState } from 'react';
import axios from "axios";
import Button from '../Button/Button'

const { COUNTER_ENDPOINT, COUNTER_KEY } = require('config.json');

function Counter() {

    const [counter, getCounter] = useState('');

    const handleClick = () => {
        axios.get(COUNTER_ENDPOINT + COUNTER_KEY).then((response => {
            const counterValue = response.data.value;
            getCounter(counterValue); 
        })).catch((error) => {
            console.log("Error: ", error);
        });
    }

    return (
        <>
        <p>Click on the button for current hits</p>
        <Button onClick={handleClick} displayLabel='HIT'></Button>
        <p>Current Hits: {counter}</p>
        </>
    );
};

export default Counter;
